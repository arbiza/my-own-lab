#include <unistd.h>
#include <sys/types.h>

#include <iostream>
#include <cstdlib>


int main ( int argc, char* argv[] ) {


    // Check usage
    if ( argc != 2 ) {
        std::cout << "\n\n  Usage:  sudo " << argv[0] << " <lab file>.json\n\n";
        return ( EXIT_FAILURE );
    }


    // Check if it has been run by with privileged rights
    uid_t euid = geteuid();
    if ( euid > 0 ) {    
        std::cout   << "\n\n  You need SUPER POWERS to make changes in system's"
            << " network; run as root or using 'sudo'!"
            << "\n\n  Usage:  sudo " << argv[0] << " <lab file>.json\n\n";
        return ( EXIT_FAILURE );
    }


    std::string cmd;

    while ( true ) {

        std::cout << "LAB > ";
        std::getline( std::cin, cmd );

        if ( cmd.compare( "start" ) == 0 ) {

        }
        else if ( cmd.compare( "stop" ) == 0 ) {

        }
        else if ( cmd.compare( "exit" ) == 0 ) {
            // stop everything
            std::cout << "\n\n    Lab stopped. Exiting.\n" << std::endl;
            break;
        }
    }
}
