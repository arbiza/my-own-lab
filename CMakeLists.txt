cmake_minimum_required(VERSION 3.5)


project(MyOwnLab)

set(EXECUTABLE my-own-lab)

set(SOURCES main.cpp)

set(HEADERS 
    # Set your headers here in "this_format.hpp" -- one per line.
    # For header included you don't need to add the source
    )

set(INCLUDE_DIR "/usr/include/")

# -----------------------------------------


# Compiler flags
add_definitions(-std=c++14 -Wall -Wno-unused-function)


# Default build type
set(CMAKE_BUILD_TYPE Release)

set( CMAKE_EXPORT_COMPILE_COMMANDS 1 )

# Messages output
message("Building project ${project}")
message("CMAKE_BUILD_TYPE is ${CMAKE_BUILD_TYPE}")


# Executable
add_executable(${EXECUTABLE} ${SOURCES})


# Link libraries -- the following commented line is an example using pthread.
# target_link_libraries(${EXECUTABLE} pthread)
